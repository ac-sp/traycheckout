<?php

class Traycheckout
{
    public static $urls = [
        'production' => 'https://checkout.tray.com.br/payment/transaction',
//        'production' => 'https://checkout.sandbox.tray.com.br/payment/transaction',
        'development' => 'https://checkout.sandbox.tray.com.br/payment/transaction',
        'staging' => 'https://checkout.sandbox.tray.com.br/payment/transaction',
    ];
    public static $tokens = [
        'production' => '858a9b0f68fa603',
//        'production' => '9a6afe9d6bccb26',
        'development' => '9a6afe9d6bccb26',
        'staging' => '9a6afe9d6bccb26',
    ];
    
    const form = 'checkout_form.php';
    protected $token_account = null;
    protected $url = null;
    protected $data = [];
    protected $notification = [];
    public $template = null;
    
    public function __construct($environment, $token_account = '') {
        $this->token_account = static::$tokens[$environment];
        $this->url = static::$urls[$environment];
        $this->template = __DIR__ . '/' . static::form;
        
        !empty($token_account) && $this->token_account = $token_account;
    }
    
    public function setProduct($desc, $unitPrice, $qtd = 1) {
        $this->data['transaction_product[][description]'] = $desc;
        $this->data['transaction_product[][quantity]'] = $qtd;
        $this->data['transaction_product[][price_unit]'] = $unitPrice;
        
        return $this;
    }
    
    public function setNotification($url) {
        $this->data['url_notification'] = $url;
        
        return $this;
    }
    
    public function setOrder($number) {
        $this->data['order_number'] = $number;
        
        return $this;
    }
    
    public function setData($key, $value) {
        $this->data[$key] = $value;
        
        return $this;
    }
    
    public function render() {
        $checkout = [
            'url' => $this->url,
            'token_account' => $this->token_account,
            'data' => $this->data
        ];
        
        ob_start();
        require($this->template);
        $contents = ob_get_contents();
        @ob_end_clean();

        return $contents;
    }
    
    public function recognizeTransactionData($notification) {
        $this->notification = (array) @$notification['transaction'];
        return $this;
    }
    
    public function identifyOrder() {
        return $this->notification['order_number'];
    }
    
    public function identifyPaymentDate() {
        return $this->notification['date_payment'];
    }
    
    public function identifyPaymentValue() {
        return $this->notification['price_payment'];
    }
    
    public function identifyPayment() {
        return [
            'order' => $this->identifyOrder(),
            'date' => $this->identifyPaymentDate(),
            'value' => $this->identifyPaymentValue(),
        ];
    }
}