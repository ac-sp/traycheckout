<form id="checkout" method="post" action="<?= $checkout['url'] ?>">

    <!-- Campos obrigatórios --> 
    <input type="hidden" name="token_account" value="<?= $checkout['token_account'] ?>"> 
    
    <?php foreach($checkout['data'] as $key => $value): ?>
    <input type="hidden" name="<?=$key?>" value="<?=$value?>"> 
    <?php endforeach; ?>
    
    <script type="text/javascript">
        require(['public/js/app'], function (App) {
            Sk.loading.modal.add('Carregando');
            $('form#checkout').submit();
        });
    </script>
</form>